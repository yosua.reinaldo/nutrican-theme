<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nutrican_2020
 */

get_header();
?>
	<?php
    echo do_shortcode('[slick-slider category="31" arrows="false" variablewidth="true" image_fit="false" autoplay="true" autoplay_interval="5000"]');
  ?>
	<main id="primary" class="site-main container pt-5 mt-3">
		<header class="page-header mb-4">
			<h1 class="text-center page-title text-dark-green mb-4"><?php single_post_title(); ?></h1>
			<div class="row">
				<div class="col-lg-6 mb-3 mb-lg-0">
					<?php
						wp_nav_menu(array(
							'menu' 				=> 23,
							'menu_class' 	=> 'pl-0 mb-0 list-unstyled d-flex justify-content-between',
							'fallback' => false,
						));
					?>
				</div>
				<div class="col-lg-6">
					<?php get_search_form(); ?>
				</div>
			</div>
		</header><!-- .page-header -->
		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			/* Start the Loop */
			echo '<div class="row masonry mb-4">';
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content-loop', get_post_type() );

			endwhile;
			echo '</div><!-- /.row -->';

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
	<?php nutrican_2020_numeric_posts_nav(); ?>
	</main><!-- #main -->

<?php
get_footer();
