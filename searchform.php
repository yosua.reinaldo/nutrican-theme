<div class="search-box">
  <form action="/" method="get" class="d-flex align-items-center">
    <button type="submit" class="btn btn-link px-3 h-100"><i class="fas fa-search py-0"></i></button>
    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Cari topik yang Anda inginkan" class="form-control search-input pl-0" />
  </form>
</div>