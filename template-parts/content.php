<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nutrican_2020
 */
	$acf = get_field('type', get_the_ID());
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta mb-4">
				<?php
				nutrican_2020_posted_on();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->
	<div class="text-center mb-4">
		<?php
			if($acf === 'Spotify') {
				echo '<iframe src="https://open.spotify.com/embed-podcast/episode/'.get_field('spotify_id', get_the_ID()).'" width="100%" height="232" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>';
			}else {
				nutrican_2020_post_thumbnail();
			}
		?>
	</div>
	<div class="d-flex justify-content-between mb-4 align-items-end">
		<?php nutrican_2020_resize_text(); ?>
		<?php nutrican_2020_share(); ?>
	</div>
	<div class="entry-content">
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'nutrican-2020' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'nutrican-2020' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .entry-content -->
	<footer class="d-flex justify-content-center justify-content-lg-end position-relative action-bottom">
		<div class="col-lg-6 row justify-content-center justify-content-lg-between">
			<?php
				if($acf === 'Seminar') {
					echo '<a href="'.get_field('link_seminar').'" target="_blank" referrer="no-referrer" class="btn btn-success align-self-end btn-daftar mr-3">Daftar</a>';
				}else {
					echo '<div></div>';
				}
				nutrican_2020_share();
			?>
		</div>
			</footer>
</article><!-- #post-<?php the_ID(); ?> -->
