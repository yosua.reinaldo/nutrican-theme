<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nutrican_2020
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('mb-4 col-6 col-lg-3 grid-item'); ?>>
  <div class="card-news">
    <?php nutrican_2020_post_thumbnail(); ?>
    <div class="entry-content py-2 px-3">
      <?php 
        $categories = get_the_category();
      ?>
      <div class="post-meta mb-2">
        <a href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) ); ?>" class="text-decoration-none text-light-green cat"><?= $categories[0]->name; ?></a>
      </div>
      <h5 class="title">
        <a href="<?php echo get_the_permalink(); ?>" class="text-decoration-none"><?php echo get_the_title(); ?></a>
      </h5>
      <span class="text-middle-grey font-weight-semi">
      <?php 
        $acf = get_field('type', get_the_ID());
        switch ($acf) {
          case 'Resep':
            echo get_field('total_kalori', get_the_ID()). ' Kalori';
            break;
          case 'Seminar':
            echo get_field('lokasi_seminar', get_the_ID());
            break;
          case 'Spotify':
            # code...
            break;
          default:
            echo 'Bacaan '.get_field('time_spent', get_the_ID()).' menit';
            break;
        }
      ?>
      </span>
    </div>
  </div>
</article><!-- #post-<?php the_ID(); ?> -->
