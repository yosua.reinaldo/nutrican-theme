<?php
/**
 * Template Name: Login Page
 */

get_header();
?>

	<main id="primary" class="site-main container pt-5 d-flex justify-content-center">
    <div class="col-lg-5">
      <?php
      while ( have_posts() ) :
        the_post();

        get_template_part( 'template-parts/content-notitle', 'page' );

      endwhile; // End of the loop.
      ?>
    </div>
	</main><!-- #main -->
<?php
get_footer();
