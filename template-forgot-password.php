<?php
/**
 * Template Name: Forgot Password Page
 */

get_header();
?>

	<main id="primary" class="site-main container pt-5">
      <?php
      if(isset($_GET['checkemail']) && $_GET['checkemail'] === 'confirm') {
        while ( have_posts() ) :
          the_post();

          get_template_part( 'template-parts/content-notitle', 'page' );

        endwhile; // End of the loop.
      }else {
        echo do_shortcode('[bbp-lost-pass]');
      }
      ?>
	</main><!-- #main -->
<?php
get_footer();
