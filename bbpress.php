<?php
/**
 * bbPress wrapper template.
 *
 * @package Nutrican BBpress
 */
global $wp;
$url = explode('/', $wp->request);
get_header(); ?>
<?php echo (!(array_key_exists(1, $url) && ($url[1] === 'users'))) ? do_shortcode('[slick-carousel-slider category="30" slidestoshow="1" image_size="full" autoplay="false" design="design-6" centermode="true" variablewidth="true" image_fit="false" arrows="false"]') : ''; ?>
<div class="container pt-4">
	<div class="row">
		<?php if(!(array_key_exists(1, $url) && ($url[1] === 'users'))): ?>
		<div class="col-lg-3 d-none d-lg-block">
			<?php
				wp_nav_menu(array(
					'menu' 				=> 24,
					'menu_class' 	=> 'ml-0 pl-0 mb-0 list-unstyled d-flex flex-nowrap flex-md-column overflow-md-auto',
					'fallback' => false,
				));
			?>
		</div>
		<?php endif; ?>
		<div class="<?php echo (!(array_key_exists(1, $url) && ($url[1] === 'users'))) ? 'col-lg-9' : 'col-lg-12'; ?>">
			<?php while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
			<?php endwhile; ?>
		</div>
	</div>
</div>
<!-- /.container -->
<?php if(!is_user_logged_in()): ?>
<div class="modal" tabindex="-1" id="loginModal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body position-relative">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-times"></i></span>
        </button>
				<?php echo do_shortcode('[bbp-login]'); ?>
      </div>
    </div>
  </div>
</div>

<script>
	( function ($) {
		$('#loginModal').modal('show')
	}(jQuery))
</script>
<?php endif; ?>
<?php get_footer();