<?php
/**
 * Template Name: Konsultasi Template
 */


get_header();
$keyword = (isset($_GET['search']) && $_GET['search'] !== '') ? sanitize_text_field($_GET['search']): '';
?>
	
	<main id="primary" class="site-main container pt-5">
		<h1 class="text-dark-green mb-4"><?php the_title(); ?></h1>
		<div class="mb-4">
			<div class="search-box">
				<form action="/konsultasi" method="get" class="d-flex align-items-center">
					<button type="submit" class="btn btn-link px-3 h-100"><i class="fas fa-search py-0"></i></button>
					<input type="text" name="search" id="search" value="<?php echo $keyword; ?>" placeholder="Cari topik yang Anda inginkan" class="form-control search-input pl-0" />
				</form>
			</div>
		</div>
		<div class="mb-4">
		<?php
			if($keyword !== '') {
				$args = array(
					'post_type' => 'faq',
					'post_status' => 'publish',
					'orderby' => 'DATE',
					'order' => 'ASC',
					's' => $keyword
				);
				$loop = new WP_Query( $args );
				nutrican_2020_list_questions($loop);
			}else {

				$cargs = array(
					'child_of'      => 0,
					'orderby'       => 'name',
					'order'         => 'ASC',
					'hide_empty'    => 1,
					'taxonomy'      => 'konsultasi_categories',
				);
				echo '<div class="accordion" id="accordionFAQ">';
				foreach(get_categories($cargs) as $key => $cat) {
					$args = array(
						'post_type' => 'konsultasi',
						'post_status' => 'publish',
						'orderby' => 'DATE',
						'order' => 'ASC',
						'tax_query' =>
							array(
								array(
									'taxonomy' => 'konsultasi_categories',
									'field'    => 'id',
									'terms'    => $cat->term_id
								),
							),
						// 's' => $keyword
					);
					echo '<div class="card">
						<div class="card-header px-0" id="heading-'.$key.'">
							<button class="btn btn-link w-100 text-left px-0 d-flex justify-content-between" type="button" data-toggle="collapse" data-target="#collapse-'.$key.'" aria-expanded="false" aria-controls="collapse-'.$key.'">
								'.$cat->name.'
								<i class="ml-3 fas fa-angle-down text-light-green"></i>
							</button>
						</div>
						<div id="collapse-'.$key.'" class="collapse" aria-labelledby="heading-'.$key.'" data-parent="#accordionFAQ">
							<div class="card-body p-0">';
					$loop = new WP_Query( $args );
					nutrican_2020_list_questions($loop);
					echo '</div></div></div>';
				}
				echo '</div>';
			}
			wp_reset_postdata();
		?>
		</div>
		<?php the_content(); ?>
	</main><!-- #main -->
<?php
get_footer();
