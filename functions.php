<?php
/**
 * nutrican 2020 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package nutrican_2020
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'nutrican_2020_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function nutrican_2020_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on nutrican 2020, use a find and replace
		 * to change 'nutrican-2020' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'nutrican-2020', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'nutrican-2020' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'nutrican_2020_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'nutrican_2020_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function nutrican_2020_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'nutrican_2020_content_width', 640 );
}
add_action( 'after_setup_theme', 'nutrican_2020_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function nutrican_2020_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'nutrican-2020' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'nutrican-2020' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Navbar Left', 'nutrican-2020' ),
			'id'            => 'navbar-left',
			'description'   => esc_html__( 'Add widgets here.', 'nutrican-2020' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Left', 'nutrican-2020' ),
			'id'            => 'footer-left',
			'description'   => esc_html__( 'Add widgets here.', 'nutrican-2020' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Right', 'nutrican-2020' ),
			'id'            => 'footer-right',
			'description'   => esc_html__( 'Add widgets here.', 'nutrican-2020' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Center', 'nutrican-2020' ),
			'id'            => 'footer-center',
			'description'   => esc_html__( 'Add widgets here.', 'nutrican-2020' ),
			'before_widget' => '<div id="%1$s" class="cta-wrapper widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'nutrican_2020_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function nutrican_2020_scripts() {
	wp_enqueue_style( 'nutrican-2020-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'nutrican-2020-style', 'rtl', 'replace' );
	wp_enqueue_style( 'nutrican-2020-bootstrap-lib', get_template_directory_uri().'/css/bootstrap.css', array(), '4.5.3');
	wp_enqueue_style( 'nutrican-2020-override-um', get_template_directory_uri().'/css/override.css', array(), '1.0.0');
	wp_enqueue_style( 'nutrican-2020-main-style', get_template_directory_uri().'/css/main.css', array(), '1.0.0');

	// Masonry
	wp_enqueue_script( 'masonry-script', get_template_directory_uri().'/node_modules/masonry-layout/dist/masonry.pkgd.min.js', array(), '4.2.2', false );

	wp_enqueue_script( 'jqvalidation-script', get_template_directory_uri().'/node_modules/jquery-validation/dist/jquery.validate.min.js', array(), '1.19.3', false );
	
	wp_enqueue_script( 'nutrican-2020-bootstrap-bundle-js', get_template_directory_uri() . '/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js', array(), '4.5.3', false );
	wp_enqueue_script( 'autosize', get_template_directory_uri().'/node_modules/autosize/dist/autosize.min.js', array(), '4.0.2', false );

	wp_enqueue_script( 'nutrican-2020-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'nutrican-2020-mainjs', get_template_directory_uri() . '/js/main.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'nutrican_2020_scripts' );

// Register Custom Post Type
function nutrican_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'FAQs', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'FAQ', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'FAQ', 'text_domain' ),
		'name_admin_bar'        => __( 'FAQ', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'FAQ', 'text_domain' ),
		'description'           => __( 'FAQ content', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'faq', $args );
	register_taxonomy( 'faq_categories', array('faq'), array(
			'hierarchical' => true, 
			'label' => 'Categories', 
			'singular_label' => 'Category', 
			'rewrite' => array( 'slug' => 'faq', 'with_front'=> false )
			)
	);

	$labelsKonsultasi = array(
		'name'                  => _x( 'Konsultasi', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Konsultasi', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Konsultasi', 'text_domain' ),
		'name_admin_bar'        => __( 'Konsultasi', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$argsKonsultasi = array(
		'label'                 => __( 'Konsultasi', 'text_domain' ),
		'description'           => __( 'Konsultasi content', 'text_domain' ),
		'labels'                => $labelsKonsultasi,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'konsultasi', $argsKonsultasi );
	register_taxonomy( 'konsultasi_categories', array('konsultasi'), array(
			'hierarchical' => true,
			'label' => 'Categories',
			'singular_label' => 'Category',
			'rewrite' => array( 'slug' => 'konsultasi', 'with_front'=> false )
			)
	);

	register_taxonomy_for_object_type( 'categories', 'work' );
}
add_action( 'init', 'nutrican_custom_post_type', 0 );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/um-override.php';
require get_template_directory() . '/inc/custom-shortcode.php';
require get_template_directory() . '/inc/bbp-functions.php';
require get_template_directory() . '/inc/custom-reset-password.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// add_action( 'user_register', 'nutrican_registration_save', 10, 99 );

function nutrican_registration_save( $user_id ) {

	if ( isset( $_POST['user_password'] ) )
		wp_set_password( $_POST['user_password'], $user_id );

	if ( isset( $_POST['first_name'] ) )
		update_user_meta($user_id, 'first_name', $_POST['first_name']);

	if ( isset( $_POST['kota'] ) )
		update_user_meta($user_id, 'kota', $_POST['kota']);
		update_user_meta($user_id, '_kota', 'field_6005b128ac8fb');

	wp_set_current_user($user_id);
	wp_set_auth_cookie($user_id);
	$user = get_user_by( 'id', $user_id );
	do_action( 'wp_login', $user->user_login );//`[Codex Ref.][1]
	wp_redirect( home_url() );
	exit;
}

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
  	show_admin_bar(false);
	}
}

function nutrican_check_fields( $errors, $sanitized_user_login, $user_email ) {
 
	$errors->add( 'demo_error', __( '<strong>ERROR</strong>: This is a demo error.', 'my_textdomain' ) );
	return $errors;
}

add_filter( 'registration_errors', 'nutrican_check_fields', 10, 3 );

add_action('wp_ajax_register_user_front_end', 'register_user_front_end', 0);
add_action('wp_ajax_nopriv_register_user_front_end', 'register_user_front_end');

function register_user_front_end() {
	$new_user_name = stripcslashes($_POST['new_user_name']);
	$new_user_email = stripcslashes($_POST['new_user_email']);
	$new_user_first_name = $_POST['first_name'];
	$new_user_password = $_POST['new_user_password'];
	$user_nice_name = strtolower($_POST['new_user_name']);
	$user_data = array(
		'user_login' => $new_user_name,
		'user_email' => $new_user_email,
		'first_name' => $new_user_first_name,
		'user_pass' => $new_user_password,
		'user_nicename' => $user_nice_name,
		'display_name' => $new_user_first_name,
		'role' => 'subscriber'
	);
	$user_id = wp_insert_user($user_data);
	if (!is_wp_error($user_id)) {
		if ( isset( $_POST['kota'] ) )
			update_user_meta($user_id, 'kota', $_POST['kota']);
			update_user_meta($user_id, '_kota', 'field_6005b128ac8fb');
		if ( isset( $_POST['jenis_kelamin'] ) )
			update_user_meta($user_id, 'jenis_kelamin', $_POST['jenis_kelamin']);
			update_user_meta($user_id, '_jenis_kelamin', 'field_6006b78b90cfd');
		if ( isset( $_POST['kelurahan'] ) )
			update_user_meta($user_id, 'kelurahan', $_POST['kelurahan']);
			update_user_meta($user_id, '_kelurahan', 'field_6006b7e890d00');
		if ( isset( $_POST['ext'] ) )
			update_user_meta($user_id, 'ext', $_POST['ext']);
			update_user_meta($user_id, '_ext', 'field_6006b7b090cfe');
		if ( isset( $_POST['no_hp'] ) )
			update_user_meta($user_id, 'no_hp', $_POST['no_hp']);
			update_user_meta($user_id, '_no_hp', 'field_6006b7c690cff');
		if ( isset( $_POST['tanggal_lahir'] ) )
			update_user_meta($user_id, 'tanggal_lahir', $_POST['tanggal_lahir']);
		if ( isset( $_POST['alamat'] ) )
			update_user_meta($user_id, 'alamat', $_POST['alamat']);
			update_user_meta($user_id, '_alamat', 'field_600408f414a32');

		wp_set_current_user($user_id);
		wp_set_auth_cookie($user_id);
		$user = get_user_by( 'id', $user_id );
		do_action( 'wp_login', $user->user_login );

		echo json_encode( array("status" => "200", "message" => "We have created an account for you.") );
		http_response_code(200);
	} else {
		$msg = '';
		if (isset($user_id->errors['empty_user_login'])) {
			$msg = 'User Name and Email are mandatory';
		} elseif (isset($user_id->errors['existing_user_login'])) {
			$msg = 'User name already exixts.';
		} else {
			$msg = 'Error Occured please fill up the sign up form carefully.';
		}
		echo json_encode( array("status" => "422", "message" => $msg) );
		http_response_code(422);
	}
	die;
}

function ajax_login_init(){

	wp_register_script('ajax-login-script', get_template_directory_uri() . '/ajax-login-script.js', array('jquery') ); 
	wp_enqueue_script('ajax-login-script');

	wp_localize_script( 'ajax-login-script', 'ajax_login_object', array( 
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'redirecturl' => home_url(),
			'loadingmessage' => __('Sending user info, please wait...')
	));

	// Enable the user with no privileges to run ajax_login() in AJAX
	add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
}

// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
	add_action('init', 'ajax_login_init');
}


function ajax_login(){

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	// Nonce is checked, get the POST data and sign user on
	$info = array();
	$info['user_login'] = $_POST['username'];
	$info['user_password'] = $_POST['password'];
	$info['remember'] = true;

	$user_signon = wp_signon( $info, false );
	if ( !is_wp_error($user_signon) ){
		wp_set_current_user($user_signon->ID);
		wp_set_auth_cookie($user_signon->ID);
		http_response_code(200);
		echo json_encode(array('loggedin'=>true, 'message'=>__('Login berhasil.')));
	}else{
		http_response_code(422);
		echo json_encode(array('loggedin'=>false, 'message'=>__('Username/Email atau Password yang Anda masukan salah.')));
	}

	die();
}
