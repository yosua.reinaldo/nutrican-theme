<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nutrican_2020
 */

?>
	<?php dynamic_sidebar('footer-center'); ?>
	</div>
	<!-- /.main-wrapper -->
	<footer id="colophon" class="site-footer">
		<!-- <div class="cta-wrapper">
			<a href="#" class="btn btn-lg btn-warning cta-beli">BELI SEKARANG</a>
		</div> -->

		<div class="container main-content">
			<div class="row">
				<div class="col-md-6 mb-4 mb-md-0">
					<?php dynamic_sidebar( 'footer-left' ); ?>
				</div>
				<!-- /.col-md-6 -->
				<div class="col-md-6">
					<?php dynamic_sidebar( 'footer-right' ); ?>
				</div>
				<!-- /.col-md-6 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php
wp_nav_menu(
	array(
		'menu' 				=> 32,
		'menu_id'        => 'mobile-menu',
		'container_class' => 'mobile-menu-wrapper d-block d-md-none',
		'menu_class' => 'menu d-flex justify-content-between m-0 pl-0 list-unstyled'
	)
);
?>
<?php nutrican_2020_mobile_share(); ?>
<?php wp_footer(); ?>

</body>
</html>
