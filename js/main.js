/* global wp, jQuery */
/**
 * File main.js.
 *
 * Theme Javascripts
 *
 * Contains scripts to enhance the output
 */

( function( $ ) {
  $.extend($.validator.messages, {
    required: '*Kolom ini wajib diisi.',
    remote: 'Harap benarkan kolom ini.',
    email: 'Silakan masukkan format email yang benar.',
    url: 'Silakan masukkan format URL yang benar.',
    date: 'Silakan masukkan format tanggal yang benar.',
    dateISO: 'Silakan masukkan format tanggal(ISO) yang benar.',
    number: 'Silakan masukkan angka yang benar.',
    digits: 'Harap masukan angka saja.',
    creditcard: 'Harap masukkan format kartu kredit yang benar.',
    equalTo: 'Password tidak sama.',
    maxlength: $.validator.format('Input dibatasi hanya {0} karakter.'),
    minlength: $.validator.format('Input tidak kurang dari {0} karakter.'),
    rangelength: $.validator.format('Panjang karakter yg diizinkan antara {0} dan {1} karakter.'),
    range: $.validator.format('Harap masukkan nilai antara {0} dan {1}.'),
    max: $.validator.format('Harap masukkan nilai lebih kecil atau sama dengan {0}.'),
    min: $.validator.format('Harap masukkan nilai lebih besar atau sama dengan {0}.'),
    alphanumeric: 'Hanya huruf, spasi dan angka.'
  });
	const ToggleReview = (() => {
    const Wrapper = $('.wp-block-lazyblock-review'),
          reviewBox = Wrapper.find('.review');
    
    reviewBox.on('click ',function() {
      $(this).addClass('show')
    })
  })

  ToggleReview()

  const MasonryNews = (() => {
    var masonryContainer = $('.masonry'),
    masonryConfig = {
      itemSelector: '.grid-item',
      percentPosition: true
    }
    $(window).on('load', function (){
      masonryContainer.masonry(masonryConfig)
    })
  })

  MasonryNews()

  const ResizeText = (() => {
    const Wrapper = $('.resize-text'),
    target = $('.entry-content');

    let defaultSize = 16;

    function resize(up) {
      defaultSize = up ? defaultSize + 2: defaultSize - 2;
      target.css('font-size', defaultSize)
    }

    Wrapper.find('.plus').on('click', function(ev) {
      ev.preventDefault();
      resize(true)
    })
    Wrapper.find('.minus').on('click', function(ev) {
      ev.preventDefault();
      resize(false)
    })
  })

  ResizeText()

  const ToggleShare = (() => {
    const Wrapper = $('.overlay-mobile-share'),
    btnShare = $('.toggle-share'),
    btnClose = Wrapper.find('.close');
    btnShare.on('click', function() {
      Wrapper.toggleClass('d-none')
      setTimeout(() => {
        Wrapper.find('.bottom-sheets').toggleClass('show')
      }, 100)
    })
    btnClose.on('click', function() {
      Wrapper.toggleClass('d-none')
      .find('.bottom-sheets').toggleClass('show')
    })

  })

  ToggleShare()

  autosize($('textarea'))

  $('.bbp-the-content').attr('placeholder', 'Tulis komentar...')

  const addClassOnScroll = (() => {
    let scrollPosition = $(window).scrollTop();
    if(scrollPosition > 150) {
      $('body').addClass('scrolled')
    }else {
      $('body').removeClass('scrolled')
    }
  })
  
  $(window).on('load scroll', function (){
    addClassOnScroll();
  })
}( jQuery ) );
