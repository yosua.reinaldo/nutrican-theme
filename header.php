<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nutrican_2020
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script src="https://kit.fontawesome.com/f7277af946.js" crossorigin="anonymous"></script>
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5fff0cf0198c72a7"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'nutrican-2020' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="container-fluid top-logo py-3 px-md-4">
			<div class="d-flex header-before justify-content-between align-items-center">
				<div class="left">
					<?php dynamic_sidebar( 'navbar-left' ); ?>
				</div>
				<div class="right d-flex align-items-center">
					<?php the_custom_logo(); ?>
				</div>
			</div>
			<!-- /.d-flex -->
		</div>
		<!-- /.container-fluid -->

		<nav id="site-navigation" class="main-navigation">
			<div class="container">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
						'container_class' => 'd-none d-md-block',
						'menu_class' => 'menu d-flex justify-content-between'
					)
				);
				?>
			</div>
			<!-- /.container -->
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
	<div class="main-wrapper">