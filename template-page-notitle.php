<?php
/**
 * Template Name: Page no title
 */


get_header();
?>

	<main id="primary" class="site-main container pt-5">
		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-notitle', 'page' );

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->
<?php
get_footer();
