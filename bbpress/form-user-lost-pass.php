<?php

/**
 * User Lost Password Form
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

?>
<h1 class="entry-title">Lupa Password</h1>
<form method="post" action="<?php bbp_wp_login_action( array( 'action' => 'lostpassword', 'context' => 'login_post' ) ); ?>" class="bbp-login-form" id="lost-pass">
	<fieldset class="bbp-form">
		<legend><?php esc_html_e( 'Lost Password', 'bbpress' ); ?></legend>

		<div class="form-group mb-5">
			<label for="user_login"><?php esc_html_e( 'Masukkan alamat email dari akun yang terdaftar', 'bbpress' ); ?></label>
			<input type="email" name="user_login" value="" size="20" id="user_login" maxlength="100" autocomplete="off" required class="form-control" />
		</div>

		<?php do_action( 'login_form', 'resetpass' ); ?>

		<div class="text-center">

			<button type="submit" name="user-submit" class="btn btn-middle-green btn-long"><?php esc_html_e( 'Reset Password', 'bbpress' ); ?></button>

			<?php bbp_user_lost_pass_fields(); ?>

		</div>
	</fieldset>
</form>
<script>
	(function($) {
		$('#lost-pass').validate();
	})(jQuery)
</script>