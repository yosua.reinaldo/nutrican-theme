<?php

/**
 * User Registration Form
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

?>

<form id="register-form" class="bbp-register-form">
	<input type="hidden" name="action" value="register_user_front_end">
	<?php //do_action( 'register_form' ); ?>
	<?php do_action( 'bbp_template_before_register_fields' ); ?>
	<div class="form-group mb-4">
		<label for="nama_lengkap">Nama Lengkap*</label>
		<input class="form-control" type="text" name="first_name" id="first_name" value="<?php bbp_sanitize_val( 'first_name' ); ?>" maxlength="100" required>
	</div>
	<div class="row justify-content-between mb-4">
		<div class="col-lg-4 form-group mb-4 mb-lg-0">
			<label for="nama_lengkap">Tanggal Lahir*</label>
			<div class="d-flex align-items-center" id="ttl">
				<input type="text" class="form-control" name="date_dob" style="max-width: 70px;" placeholder="DD" maxlength="2" minlength="2" required number="true">
				<span class="mx-2 separator">-</span>
				<input type="text" class="form-control" name="month_dob" style="max-width: 70px;" placeholder="MM" maxlength="2" minlength="2" required number="true">
				<span class="mx-2 separator">-</span>
				<input type="text" class="form-control" name="year_dob" style="max-width: 90px;" placeholder="YYYY" maxlength="4" minlength="4" required number="true">
			</div>
			<div id="errorTTL"></div>
			<label id="errorTTLNotValid" class="d-none error">*Tanggal tidak valid.</label>
		</div>
		<div class="px-3">
			<label for="nama_lengkap" class="d-block">Jenis Kelamin*</label>
			<div class="btn-group-toggle input-radio-button" data-toggle="buttons">
				<label class="btn btn-outline active mr-3">
					<input type="radio" name="jenis_kelamin" id="wanita" value="wanita" required> Wanita
				</label>
				<label class="btn btn-outline">
					<input type="radio" name="jenis_kelamin" id="laki_laki" value="laki_laki"> Laki-Laki
				</label>
			</div>
			<label for="jenis_kelamin" class="error"></label>
		</div>
	</div>
	<label for="nomor_hp">Nomor Handphone*</label>
	<div class="mb-4">
		<div class="d-flex align-items-center">
			<input type="text" class="form-control" style="max-width: 70px;" placeholder="+62" maxlength="3" name="ext">
			<span class="mx-2 separator">-</span>
			<input type="text" class="form-control" placeholder="8XX - XXXX - XXXX" name="no_hp" required number="true">
		</div>
		<label for="no_hp" class="error"></label>
	</div>
	<div class="form-group mb-4">
		<label for="alamat">Alamat*</label>
		<input class="form-control" type="text" name="alamat" required>
	</div>
	<div class="row mb-4">
		<div class="form-group col-lg-6 mb-4 mb-lg-0">
			<label for="kota">Kota/Kabupaten*</label>
			<input class="form-control" name="kota" id="kota" type="text" required>
		</div>
		<div class="form-group col-lg-6 mb-0">
			<label for="kelurahan">Kelurahan/Desa*</label>
			<input class="form-control" name="kelurahan" id="kelurahan" type="text" required>
		</div>
	</div>
	<div class="form-group mb-4">
		<label for="user_name">Username*</label>
		<input class="form-control" type="text" name="user_name" value="<?php bbp_sanitize_val( 'user_name' ); ?>" size="20" id="user_name" maxlength="100" autocomplete="off" required />
	</div>
	<div class="form-group mb-4">
		<label for="user_email">Email*</label>
		<input class="form-control" type="text" name="user_email" value="<?php bbp_sanitize_val( 'user_email' ); ?>" size="20" id="user_email" maxlength="100" autocomplete="off" required email="true" />
	</div>
	<div class="row mb-4">
		<div class="form-group col-lg-6 mb-4 mb-lg-0">
			<label for="password">Password* (terdiri dari huruf kapital, angka dan tanda baca)</label>
			<input class="form-control" type="password" name="user_password" value="" id="user_password" class="input" required />
		</div>
		<div class="form-group col-lg-6 mb-0">
			<label for="confirm_password">Ulangi Password*</label>
			<input class="form-control" type="password" name="password1" value="" id="password1" class="input" required equalTo="#user_password" />
		</div>
	</div>

	<div class="text-center my-5">
		<button type="submit" name="user-submit" class="btn btn-middle-green btn-lg submit user-submit" id="register-button"><?php esc_html_e( 'Register', 'bbpress' ); ?></button>
		<?php bbp_user_register_fields(); ?>
	</div>

	<?php do_action( 'bbp_template_after_register_fields' ); ?>
</form>

<script>
	( function( $ ) {
		var inputTTL = $('#ttl input[type="text"]');
		var dateRegex = /^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-.\/])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;
		inputTTL.on('input', function() {
			var validateDate = '';
			inputTTL.each(function() {
				validateDate += $(this).val()+'/'
			})
			validateDate = validateDate.slice(0, -1)
			if(validateDate.length > 9) {
				if(!dateRegex.test(validateDate)) {
					setTimeout(() => {
						inputTTL.each(function() {
							$(this).val('')
						})
					}, 1000);
					$('#errorTTLNotValid').removeClass('d-none')
				}else{
					$('#errorTTLNotValid').addClass('d-none')
				}
			}
		})
		$('#register-form').validate({
			submitHandler: function(form) {
				var newUserName = $('#user_name').val(),
				newUserEmail = $('#user_email').val(),
				newUserPassword = $('#user_password').val(),
				fieldKota = $('#kota').val();
				var data = $(this).serialize()
				function submit() {
					$.ajax({
						type:"POST",
						url:"<?php echo admin_url('admin-ajax.php'); ?>",
						data: {
							action: "register_user_front_end",
							first_name: $('input[name="first_name"]').val(),
							new_user_name : $('#user_name').val(),
							new_user_email : $('#user_email').val(),
							new_user_password : $('#user_password').val(),
							kota: $('input[name="kota"]').val(),
							kelurahan: $('input[name="kelurahan"]').val(),
							alamat: $('input[name="alamat"]').val(),
							jenis_kelamin: $('input[name="jenis_kelamin"]').val(),
							ext: $('input[name="ext"]').val(),
							no_hp: $('input[name="no_hp"]').val(),
						},
						success: function(results){
							window.location = '<?php echo site_url('/discussions'); ?>'
						},
						error: function(results) {
							console.log(results)
						}
					});
				}
				submit()
			},
			errorPlacement: function(error, element) {
				var n = element.attr("name");
				if (n == "date_dob" || n == "month_dob" || n == "year_dob") {

					$('#errorTTL').html(error);
				}else {
					error.insertAfter(element);
				}
			}
		});
	}( jQuery ) );
</script>