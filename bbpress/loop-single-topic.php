<?php

/**
 * Topics Loop - Single
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

?>

<div id="bbp-topic-<?php bbp_topic_id(); ?>" <?php bbp_topic_class(''); ?>>
	<div class="bbp-topic-title">

		<?php if ( bbp_is_user_home() ) : ?>

			

		<?php endif; ?>
		<?php do_action( 'bbp_theme_before_topic_started_by' ); ?>
		<div class="d-flex justify-content-between mb-3">
			<div class="bbp-topic-started-by d-flex align-items-center"><?php echo nutrican_2020_get_topic_author_link( array( 'size' => '50' ) ); ?></div>
			<?php bbp_topic_favorite_link(); ?>
		</div>
		<?php do_action( 'bbp_theme_before_topic_title' ); ?>
		<a class="bbp-topic-permalink" href="<?php bbp_topic_permalink(); ?>"><?php bbp_topic_title(); ?></a>
		<div class="content">
			<?php 
				$total_words = str_word_count( strip_tags( get_the_content() ) );
				the_content();
			?>
		</div>
		<?php do_action( 'bbp_theme_after_topic_title' ); ?>

		<?php do_action( 'bbp_theme_before_topic_meta' ); ?>

		<div class="bbp-topic-meta mb-3 font-14">

			<?php do_action( 'bbp_theme_after_topic_started_by' ); ?>

			<?php if ( ! bbp_is_single_forum() || ( bbp_get_topic_forum_id() !== bbp_get_forum_id() ) ) : ?>

				<?php do_action( 'bbp_theme_before_topic_started_in' ); ?>

				<span class="bbp-topic-started-in"><?php printf( esc_html__( 'in: %1$s', 'bbpress' ), '<a href="' . bbp_get_forum_permalink( bbp_get_topic_forum_id() ) . '">' . bbp_get_forum_title( bbp_get_topic_forum_id() ) . '</a>' ); ?></span>
				<?php do_action( 'bbp_theme_after_topic_started_in' ); ?>

			<?php endif; ?>

		</div>

		<?php do_action( 'bbp_theme_after_topic_meta' ); ?>
		<div class="mb-2">
			<?php bbp_topic_row_actions(); ?>
		</div>

	</div>
	<!-- /.bbp-topic-title -->
	<div class="d-flex justify-content-between">
		<div class="d-flex align-items-center">
			<div class="mr-2">
				<?php echo do_shortcode('[wp_ulike]'); ?>
			</div>
			<div class="bbp-topic-reply-count">
				<button class="btn btn-link toggle-reply" data-toggle="collapse" data-target="#replies-<?php echo get_the_ID(); ?>" role="button" aria-expanded="false" aria-controls="replies-<?php echo get_the_ID(); ?>">
					<?php bbp_topic_reply_count(); ?>
				</button>
			</div>
		</div>
	</div>
	<div class="replies collapse mt-3" id="replies-<?php echo get_the_ID(); ?>">
		<div class="d-flex reply-box-wrapper">
			<div class="avatar mr-3">
				<?php echo get_avatar(get_current_user_ID(), 45); ?>
			</div>
			<div class="reply-box w-100">
				<?php bbp_get_template_part( 'form', 'reply-loop' ); ?>
			</div>
		</div>
		<ul class="reply-list unstyled-list">
		<?php
			$args = array(
				'numberposts' => 3,
				'post_parent' => get_the_ID(),
				'post_type' => 'reply'
			);

			$replies = get_children($args);
			foreach($replies as $reply) {
				$author = get_the_author_meta('first_name', $reply->post_author);
				echo '<li class="mb-3 pb-3 border-bottom"><div class="d-flex">';
				echo '<div class="avatar mr-3">'.get_avatar($reply->post_author, 45).'</div>';
				echo '<div class="comment w-100">';
				echo '<span class="firstname text-dark-grey font-weight-bold mb-2 d-block">'.$author.'</span>';
				echo '<div class="reply-content mb-3">'.wp_strip_all_tags($reply->post_content).'</div>';
				echo '<span class="font-14 font-weight-medium text-middle-grey">'.date(get_option('date_format'), strtotime($reply->post_date)).'</span>';
				echo '</div>';
				echo '</li>';
			}
		?>
		</ul>
		<div class="text-center pb-3">
			<a href="<?php bbp_topic_permalink(); ?>" class="font-weight-bold text-light-green">Lihat komentar lain (<?php bbp_topic_reply_count(); ?>)</a>
		</div>
	</div>

</div><!-- #bbp-topic-<?php bbp_topic_id(); ?> -->
