<?php

/**
 * Search 
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;
$current_user = wp_get_current_user();
if ( bbp_allow_search() ) : ?>
	<div class="form-group d-flex" id="bbp-search-form">
		<form role="search" method="get" class="w-100">
			<label class="screen-reader-text hidden" for="bbp_search"><?php esc_html_e( 'Search for:', 'bbpress' ); ?></label>
			<input type="hidden" name="action" value="bbp-search-request" />
			<input type="text" value="<?php bbp_search_terms(); ?>" name="bbp_search" id="bbp_search" class="form-control w-100" placeholder="Cari topik atau pertanyaan" />
		</form>
		<?php if(!is_user_logged_in()): ?>
			<button class="btn btn-outline-success btn-show-login ml-3" data-toggle="modal" data-target="#loginModal"><i class="far fa-user d-block"></i>Masuk</button>
		<?php else: ?>
			<a class="btn btn-outline-success btn-show-login ml-3 d-flex align-items-center" href="<?php echo site_url('/discussions/users/'.$current_user->user_nicename); ?>">
				<span>
					<i class="far fa-user d-block"></i>Profile
				</span>
			</a>
		<?php endif; ?>
	</div>

<?php endif;
