<?php

/**
 * User Login Form
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

?>

<form method="post" action="<?php bbp_wp_login_action( array( 'context' => 'login_post' ) ); ?>" class="bbp-login-form">
	<fieldset class="bbp-form mr-0 w-100">
		<h4 class="text-center text-dark-green mb-4">Masuk</h4>
		<div class="alert alert-warning d-none" role="alert"></div>
		<div class="form-group mb-4">
			<input placeholder="Email" type="email" name="log" value="<?php bbp_sanitize_val( 'user_login', 'text' ); ?>" size="20" maxlength="100" id="user_login" autocomplete="off" class="form-control" required/>
		</div>

		<div class="form-group mb-4">
			<input placeholder="Password" type="password" name="pwd" value="<?php bbp_sanitize_val( 'user_pass', 'password' ); ?>" size="20" id="user_pass" autocomplete="off" required class="form-control" />
		</div>

		<?php do_action( 'login_form' ); ?>

		<div class="text-center text-dark-grey">
			<div class="mb-4">
				<a href="<?php echo home_url(); ?>/forgot-password" class="font-weight-bold text-dark-green font-14">Lupa Password</a>
			</div>
			<div class="border-bottom border-middle-grey pb-4 mb-4">
				<button type="submit" name="user-submit" id="user-submit" class="btn btn-lg btn-middle-green btn-long"><?php esc_html_e( 'Masuk', 'bbpress' ); ?></button>
			</div>
			<p class="font-weight-bold">Belum punya akun Nutrican?</p>
			<a href="<?php echo home_url(); ?>/daftar" class="font-weight-bold text-dark-green">Daftar</a>
			<?php bbp_user_login_fields(); ?>
			<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
		</div>
	</fieldset>
</form>

<script>
	( function( $ ) {
		$('.bbp-login-form').validate({
			submitHandler: function(form) {
				$.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_login_object.ajaxurl,
            data: {
							'action': 'ajaxlogin',
							'username': $('#user_login').val(),
							'password': $('#user_pass').val(),
							'security': $('#security').val()
						},
            success: function(data){
							$('.alert-warning').addClass('d-none');
							window.location = '<?php echo site_url(); ?>/discussions'
						},
						error: function(data) {
							$('.alert-warning').removeClass('d-none').text(data.responseJSON.message);
						}
        });
			}
		});
	}( jQuery ) );
</script>
