<?php

/**
 * Replies Loop - Single Reply
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

?>

<div id="post-<?php bbp_reply_id(); ?>" class="bbp-reply-header">
	<div class="bbp-meta">

		<?php if ( bbp_is_single_user_replies() ) : ?>

			<span class="bbp-header">
				<?php esc_html_e( 'in reply to: ', 'bbpress' ); ?>
				<a class="bbp-topic-permalink" href="<?php bbp_topic_permalink( bbp_get_reply_topic_id() ); ?>"><?php bbp_topic_title( bbp_get_reply_topic_id() ); ?></a>
			</span>

		<?php endif; ?>

		<a href="<?php bbp_reply_url(); ?>" class="bbp-reply-permalink">#<?php bbp_reply_id(); ?></a>

		<?php do_action( 'bbp_theme_before_reply_admin_links' ); ?>

		<?php bbp_reply_admin_links(); ?>

		<?php do_action( 'bbp_theme_after_reply_admin_links' ); ?>

	</div><!-- .bbp-meta -->
</div><!-- #post-<?php bbp_reply_id(); ?> -->

<div <?php bbp_reply_class(); ?>>
	<div class="d-flex justify-content-between mb-3">
		<div class="bbp-topic-started-by d-flex align-items-center"><?php echo nutrican_2020_get_topic_author_link( array( 'size' => '50' ) ); ?></div>
	</div>
	<?php do_action( 'bbp_theme_before_reply_content' ); ?>
	<h6 class="bbp-topic-permalink"><?php bbp_topic_title(); ?></h6>
	<div class="content">
		<?php bbp_reply_content(); ?>
		<div class="d-flex justify-content-between">
			<div class="d-flex align-items-center">
				<div class="mr-2">
					<?php echo do_shortcode('[wp_ulike]'); ?>
				</div>
			</div>
		</div>
	</div>
</div><!-- .reply -->
