<?php
/**
 * Template Name: User Profile
 *
 * Allow users to update their profiles from Frontend.
 *
 */

/* Get user info. */
global $current_user, $wp_roles;
global $blog_id, $post, $wpdb, $wp_user_avatar, $wpua_resize_crop, $wpua_resize_h, $wpua_resize_upload, $wpua_resize_w;

/* Load the registration file. */
$error = array();
/* If profile was saved, update profile. */
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    require_once(ABSPATH . 'wp-admin/includes/file.php' );
    require_once(ABSPATH . 'wp-admin/includes/media.php' );
    /* Update user password. */
    if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
        if ( $_POST['pass1'] == $_POST['pass2'] )
            wp_update_user( array( 'ID' => get_current_user_id(), 'user_pass' => esc_attr( $_POST['pass1'] ) ) );
        else
            $error[] = __('The passwords you entered do not match.  Your password was not updated.', 'profile');
    }
    /* Update user information. */
    if ( !empty( $_POST['url'] ) )
      wp_update_user( array( 'ID' => get_current_user_id(), 'user_url' => esc_url( $_POST['url'] ) ) );
    if ( !empty( $_POST['email'] ) ){
        if (!is_email(esc_attr( $_POST['email'] )))
          $error[] = __('The Email you entered is not valid.  please try again.', 'profile');
        elseif(email_exists(esc_attr( $_POST['email'] )) != get_current_user_id() )
          $error[] = __('This email is already used by another user.  try a different one.', 'profile');
        else{
            wp_update_user( array ('ID' => get_current_user_id(), 'user_email' => esc_attr( $_POST['email'] )));
        }
    }

    if( $_FILES['wpua-file']['size'] !== 0) {
      if (current_user_can('administrator')) {
        update_user_meta(wp_get_current_user()->id, "wp_user_avatar", $_POST['wp-user-avatar']);
      } else {
        $upload = wp_upload_bits( $_FILES['wpua-file']['name'], null, file_get_contents( $_FILES['wpua-file']['tmp_name'] ) );
        $wp_filetype = wp_check_filetype( basename( $upload['file'] ), null );
        $wp_upload_dir = wp_upload_dir();
        $attachment = array(
            'guid' => $wp_upload_dir['baseurl'] .'/'. _wp_relative_upload_path( $upload['file'] ),
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename( $upload['file'] )),
            'post_content'   => '',
            'post_status'    => 'inherit'
        );
        $attach_id = wp_insert_attachment( $attachment, $upload['file']);
        $attach_data = wp_generate_attachment_metadata( $attach_id, $upload['file'] );
        wp_update_attachment_metadata( $attach_id, $attach_data );
        update_user_meta(get_current_user_id(), "wp_user_avatar", $attach_id);
      }
    }


    if ( !empty( $_POST['first_name'] ) )
        update_user_meta( get_current_user_id(), 'first_name', esc_attr( $_POST['first_name'] ) );
    if ( !empty( $_POST['alamat'] ) )
        update_user_meta( get_current_user_id(), 'alamat', esc_attr( $_POST['alamat'] ) );
    if ( !empty( $_POST['description'] ) )
        update_user_meta( get_current_user_id(), 'description', esc_attr( $_POST['description'] ) );
    if ( !empty( $_POST['date_dob'] ) && !empty( $_POST['month_dob'] ) && !empty( $_POST['year_dob'] ) )
      update_user_meta( get_current_user_id(), 'tanggal_lahir', esc_attr( $_POST['year_dob'] . $_POST['month_dob'] . $_POST['date_dob'] ) );
    if ( !empty( $_POST['jenis_kelamin'] ) )
        update_user_meta( get_current_user_id(), 'jenis_kelamin', esc_attr( $_POST['jenis_kelamin'] ) );
    if ( !empty( $_POST['ext'] ) )
        update_user_meta( get_current_user_id(), 'ext', esc_attr( $_POST['ext'] ) );
    if ( !empty( $_POST['no_hp'] ) )
        update_user_meta( get_current_user_id(), 'no_hp', esc_attr( $_POST['no_hp'] ) );
    if ( !empty( $_POST['kota'] ) )
        update_user_meta( get_current_user_id(), 'kota', esc_attr( $_POST['kota'] ) );
    if ( !empty( $_POST['kelurahan'] ) )
        update_user_meta( get_current_user_id(), 'kelurahan', esc_attr( $_POST['kelurahan'] ) );
    /* Redirect so the page will show updated info.*/
    /*I am not Author of this Code- i dont know why but it worked for me after changing below line to if ( count($error) == 0 ){ */
    if ( count($error) == 0 ) {
        //action hook for plugins and extra fields saving
        do_action('edit_user_profile_update', get_current_user_id());
        wp_redirect( get_permalink() );
        exit;
    }
}
get_header();

?>

	<main id="primary" class="site-main container">
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>">
      <div class="entry-content entry pt-5">
          <?php the_content(); ?>
          <?php if ( !is_user_logged_in() ) : ?>
            <p class="warning">
              <?php _e('You must be logged in to edit your profile.', 'profile'); ?>
            </p><!-- .warning -->
          <?php else : ?>
            <h1 class="text-dark-green font-weight-bold mb-4 text-center">Profil</h1>
            <?php if ( count($error) > 0 ) echo '<div class="alert alert-danger">' . implode("<br />", $error) . '</div>'; ?>
            <form method="post" id="editprofile" action="<?php the_permalink(); ?>" enctype='multipart/form-data'>
              <header class="profile-picture d-flex align-items-center mb-5">
                <div class="update-pp mx-auto d-flex align-items-center">
                  <div class="avatar d-flex align-items-center position-relative rounded-circle">
                    <img src="<?php echo get_avatar_url(get_current_user_id()); ?>" id="avatar" class="rounded-circle w-100" />
                    <label for="file" class="btn btn-tertiary js-labelFile px-0 h-100 w-100 d-flex align-items-center">
                      <span class="js-fileName text-blue d-block w-100"><i class="fas fa-camera"></i></span>
                    </label>
                    <div class="form-group">
                      <input type="file" name="wpua-file" id="file" class="input-file">
                    </div>
                  </div>
                </div>
              </header>
              <div class="form-group mb-4">
                <label for="nama_lengkap">Nama Lengkap</label>
                <input class="form-control" type="text" name="first_name" id="first_name" value="<?php the_author_meta( 'first_name', get_current_user_id() ); ?>" maxlength="100" required>
              </div>
              <div class="form-group mb-4">
                <label for="nama_lengkap">Status</label>
                <textarea required class="form-control" type="text" name="description" id="description" cols="30" rows="5"><?php the_author_meta( 'description', get_current_user_id() ); ?></textarea>
              </div>
              <div class="row justify-content-between mb-4">
                <div class="col-lg-4 form-group mb-4 mb-lg-0">
                  <label for="nama_lengkap">Tanggal Lahir*</label>
                  <?php 
                    $date = get_the_author_meta('tanggal_lahir', get_current_user_id());
                  ?>
                  <div class="d-flex align-items-center" id="ttl">
                    <input required type="text" class="form-control" name="date_dob" style="max-width: 70px;" placeholder="DD" maxlength="2" minlength="2"  number="true" value="<?php echo date_i18n('d', strtotime($date)); ?>">
                    <span class="mx-2 separator">-</span>
                    <input required type="text" class="form-control" name="month_dob" style="max-width: 70px;" placeholder="MM" maxlength="2" minlength="2"  number="true" value="<?php echo date_i18n('m', strtotime($date)); ?>">
                    <span class="mx-2 separator">-</span>
                    <input required type="text" class="form-control" name="year_dob" style="max-width: 90px;" placeholder="YYYY" maxlength="4" minlength="4"  number="true" value="<?php echo date_i18n('Y', strtotime($date)); ?>">
                    <input type="hidden" id="validate_date">
                  </div>
                  <div id="errorTTL"></div>
                  <label id="errorTTLNotValid" class="d-none error">Tanggal tidak valid.</label>
                </div>
                <div class="px-3">
                  <label for="jenis_kelamin" class="d-block">Jenis Kelamin*</label>
                  <div class="btn-group-toggle input-radio-button" data-toggle="buttons">
                    <label class="btn btn-outline mr-3"> Wanita
                      <input type="radio" name="jenis_kelamin" id="wanita" value="wanita" <?php if(get_the_author_meta( 'jenis_kelamin', get_current_user_id() ) === 'wanita') echo 'checked'; ?>>
                    </label>
                    <label class="btn btn-outline">
                      <input type="radio" name="jenis_kelamin" id="laki_laki" value="laki_laki" <?php if(get_the_author_meta( 'jenis_kelamin', get_current_user_id() ) === 'laki_laki') echo 'checked'; ?>> Laki-Laki
                    </label>
                  </div>
                  <label for="gender" class="error"></label>
                </div>
              </div>
              <label for="nomor_hp">Nomor Handphone*</label>
              <div class="mb-4">
                <div class="d-flex align-items-center">
                  <input type="text" class="form-control" style="max-width: 70px;" placeholder="+62" maxlength="3" name="ext" value="<?php the_author_meta( 'ext', get_current_user_id() ); ?>">
                  <span class="mx-2 separator">-</span>
                  <input type="text" class="form-control" placeholder="8XX - XXXX - XXXX" name="no_hp" required number="true" value="<?php the_author_meta( 'no_hp', get_current_user_id() ); ?>">
                </div>
                <label for="no_hp" class="error"></label>
              </div>
              <div class="form-group mb-4">
                <label for="alamat">Alamat*</label>
                <input class="form-control" type="text" name="alamat" required value="<?php the_author_meta( 'alamat', get_current_user_id() ); ?>">
              </div>
              <div class="row mb-4">
                <div class="form-group col-lg-6 mb-4 mb-lg-0">
                  <label for="kota">Kota/Kabupaten*</label>
                  <input class="form-control" name="kota" id="kota" type="text" required value="<?php the_author_meta( 'kota', get_current_user_id() ); ?>">
                </div>
                <div class="form-group col-lg-6 mb-0">
                  <label for="kelurahan">Kelurahan/Desa*</label>
                  <input class="form-control" name="kelurahan" id="kelurahan" type="text" required value="<?php the_author_meta( 'kelurahan', get_current_user_id() ); ?>">
                </div>
              </div>
              <div class="form-group mb-4">
                <label for="user_email">Email*</label>
                <input class="form-control" type="text" name="user_email" value="<?php the_author_meta( 'user_email', get_current_user_id() ); ?>" size="20" id="user_email" maxlength="100" autocomplete="off" required email="true" />
              </div>
              <div class="row mb-4">
                <div class="form-group col-lg-6 mb-4 mb-lg-0">
                  <label for="password">Password* (terdiri dari huruf kapital, angka dan tanda baca)</label>
                  <input class="form-control" name="pass1" type="password" id="pass1" />
                </div>
                <div class="form-group col-lg-6 mb-0">
                  <label for="confirm_password">Ulangi Password*</label>
                  <input class="form-control" name="pass2" type="password" id="pass2" equalTo="#pass1"/>
                </div>
              </div>
              <div class="text-center mt-5 pt-3">
                <button type="submit" class="btn btn-middle-green btn-lg submit user-submit" id="wpua-upload-existing" name="submit" value="Upload">Simpan</button>
                <?php wp_nonce_field( 'update-user' ) ?>
                <input name="action" type="hidden" id="action" value="update-user" />
              </div>
            </form><!-- #adduser -->
          <?php endif; ?>
        </div><!-- .entry-content -->
    </div><!-- .hentry .post -->
    <?php endwhile; ?>
    <?php else: ?>
        <p class="no-data">
            <?php _e('Sorry, no page matched your criteria.', 'profile'); ?>
        </p><!-- .no-data -->
    <?php endif; ?>

	</main><!-- #main -->
  <script>
    (function ($){
      var inputTTL = $('#ttl input[type="text"]');
      var dateRegex = /^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-.\/])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;
      inputTTL.on('input', function() {
        var validateDate = '';
        inputTTL.each(function() {
          validateDate += $(this).val()+'/'
        })
        validateDate = validateDate.slice(0, -1)
        if(validateDate.length > 9) {
          if(!dateRegex.test(validateDate)) {
            setTimeout(() => {
              inputTTL.each(function() {
                $(this).val('')
              })
            }, 1000);
            $('#errorTTLNotValid').removeClass('d-none')
          }else{
            $('#errorTTLNotValid').addClass('d-none')
          }
        }
      })
      $('#editprofile').validate({
        errorPlacement: function(error, element) {
          var n = element.attr("name");
          if (n == "date_dob" || n == "month_dob" || n == "year_dob") {

            $('#errorTTL').html(error);
          }else {
            error.insertAfter(element);
          }
        }
      });
      $("#file").change(function() {
        var $input = $(this);
          var inputFiles = this.files;
          if(inputFiles == undefined || inputFiles.length == 0) return;
          var inputFile = inputFiles[0];

          var reader = new FileReader();
          reader.onload = function(event) {
              $('#avatar').attr("src", event.target.result);
          };
          reader.onerror = function(event) {
              alert("ERROR: " + event.target.error.code);
          };
          reader.readAsDataURL(inputFile);
      });
      $('.input-file').each(function() {
        var $input = $(this),
            $label = $input.next('.js-labelFile'),
            labelVal = $label.html();
        $input.on('change', function(element) {
            var fileName = '';
            if (element.target.value) fileName = element.target.value.split('\\').pop();
            fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
        });
      });
    }(jQuery))
  
  </script>
<?php
get_footer();

