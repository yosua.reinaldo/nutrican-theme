<?php
  function um_profile_custom( $args ) {
    $classes = null;
  
    if ( ! $args['cover_enabled'] ) {
      $classes .= ' no-cover';
    }
  
    $default_size = str_replace( 'px', '', $args['photosize'] );
  
    // Switch on/off the profile photo uploader
    $disable_photo_uploader = empty( $args['use_custom_settings'] ) ? UM()->options()->get( 'disable_profile_photo_upload' ) : $args['disable_photo_upload'];
  
    if ( ! empty( $disable_photo_uploader ) ) {
      $args['disable_photo_upload'] = 1;
      $overlay = '';
    } else {
      $overlay = '<span class="um-profile-photo-overlay">
        <span class="um-profile-photo-overlay-s">
          <ins>
            <i class="um-faicon-camera"></i>
          </ins>
        </span>
      </span>';
    } ?>
  
    <div class="um-header<?php echo esc_attr( $classes ); ?>">
  
      <?php
      /**
       * UM hook
       *
       * @type action
       * @title um_pre_header_editprofile
       * @description Insert some content before edit profile header
       * @input_vars
       * [{"var":"$args","type":"array","desc":"Form Arguments"}]
       * @change_log
       * ["Since: 2.0"]
       * @usage add_action( 'um_pre_header_editprofile', 'function_name', 10, 1 );
       * @example
       * <?php
       * add_action( 'um_pre_header_editprofile', 'my_pre_header_editprofile', 10, 1 );
       * function my_pre_header_editprofile( $args ) {
       *     // your code here
       * }
       * ?>
       */
      do_action( 'um_pre_header_editprofile', $args ); ?>
  
      <div class="um-profile-photo" data-user_id="<?php echo esc_attr( um_profile_id() ); ?>">
  
        <a href="<?php echo esc_url( um_user_profile_url() ); ?>" class="um-profile-photo-img" title="<?php echo esc_attr( um_user( 'display_name' ) ); ?>">
          <?php if ( ! $default_size || $default_size == 'original' ) {
            $profile_photo = UM()->uploader()->get_upload_base_url() . um_user( 'ID' ) . "/" . um_profile( 'profile_photo' );
  
            $data = um_get_user_avatar_data( um_user( 'ID' ) );
            echo $overlay . sprintf( '<img src="%s" class="%s" alt="%s" data-default="%s" onerror="%s" />',
              esc_url( $profile_photo ),
              esc_attr( $data['class'] ),
              esc_attr( $data['alt'] ),
              esc_attr( $data['default'] ),
              'if ( ! this.getAttribute(\'data-load-error\') ){ this.setAttribute(\'data-load-error\', \'1\');this.setAttribute(\'src\', this.getAttribute(\'data-default\'));}'
            );
          } else {
            echo $overlay . get_avatar( um_user( 'ID' ), $default_size );
          } ?>
        </a>
  
        <?php if ( empty( $disable_photo_uploader ) && empty( UM()->user()->cannot_edit ) ) {
  
          UM()->fields()->add_hidden_field( 'profile_photo' );
  
          if ( ! um_profile( 'profile_photo' ) ) { // has profile photo
  
            $items = array(
              '<a href="javascript:void(0);" class="um-manual-trigger" data-parent=".um-profile-photo" data-child=".um-btn-auto-width">' . __( 'Upload photo', 'ultimate-member' ) . '</a>',
              '<a href="javascript:void(0);" class="um-dropdown-hide">' . __( 'Cancel', 'ultimate-member' ) . '</a>',
            );
  
            /**
             * UM hook
             *
             * @type filter
             * @title um_user_photo_menu_view
             * @description Change user photo on menu view
             * @input_vars
             * [{"var":"$items","type":"array","desc":"User Photos"}]
             * @change_log
             * ["Since: 2.0"]
             * @usage
             * <?php add_filter( 'um_user_photo_menu_view', 'function_name', 10, 1 ); ?>
             * @example
             * <?php
             * add_filter( 'um_user_photo_menu_view', 'my_user_photo_menu_view', 10, 1 );
             * function my_user_photo_menu_view( $items ) {
             *     // your code here
             *     return $items;
             * }
             * ?>
             */
            $items = apply_filters( 'um_user_photo_menu_view', $items );
  
            UM()->profile()->new_ui( 'bc', 'div.um-profile-photo', 'click', $items );
  
          } elseif ( UM()->fields()->editing == true ) {
  
            $items = array(
              '<a href="javascript:void(0);" class="um-manual-trigger" data-parent=".um-profile-photo" data-child=".um-btn-auto-width">' . __( 'Change photo', 'ultimate-member' ) . '</a>',
              '<a href="javascript:void(0);" class="um-reset-profile-photo" data-user_id="' . esc_attr( um_profile_id() ) . '" data-default_src="' . esc_url( um_get_default_avatar_uri() ) . '">' . __( 'Remove photo', 'ultimate-member' ) . '</a>',
              '<a href="javascript:void(0);" class="um-dropdown-hide">' . __( 'Cancel', 'ultimate-member' ) . '</a>',
            );
  
            /**
             * UM hook
             *
             * @type filter
             * @title um_user_photo_menu_edit
             * @description Change user photo on menu edit
             * @input_vars
             * [{"var":"$items","type":"array","desc":"User Photos"}]
             * @change_log
             * ["Since: 2.0"]
             * @usage
             * <?php add_filter( 'um_user_photo_menu_edit', 'function_name', 10, 1 ); ?>
             * @example
             * <?php
             * add_filter( 'um_user_photo_menu_edit', 'my_user_photo_menu_edit', 10, 1 );
             * function my_user_photo_menu_edit( $items ) {
             *     // your code here
             *     return $items;
             * }
             * ?>
             */
            $items = apply_filters( 'um_user_photo_menu_edit', $items );
  
            UM()->profile()->new_ui( 'bc', 'div.um-profile-photo', 'click', $items );
  
          }
  
        } ?>
  
      </div>
  
      <div class="um-profile-meta">
  
        <?php
        /**
         * UM hook
         *
         * @type action
         * @title um_before_profile_main_meta
         * @description Insert before profile main meta block
         * @input_vars
         * [{"var":"$args","type":"array","desc":"Form Arguments"}]
         * @change_log
         * ["Since: 2.0.1"]
         * @usage add_action( 'um_before_profile_main_meta', 'function_name', 10, 1 );
         * @example
         * <?php
         * add_action( 'um_before_profile_main_meta', 'my_before_profile_main_meta', 10, 1 );
         * function my_before_profile_main_meta( $args ) {
         *     // your code here
         * }
         * ?>
         */
        do_action( 'um_before_profile_main_meta', $args ); ?>
  
        <div class="um-main-meta">
  
          <?php if ( $args['show_name'] ) { ?>
            <div class="um-name">
  
              <a href="<?php echo esc_url( um_user_profile_url() ); ?>"
                  title="<?php echo esc_attr( um_user( 'display_name' ) ); ?>"><?php echo um_user( 'display_name', 'html' ); ?></a>
  
              <?php
              /**
               * UM hook
               *
               * @type action
               * @title um_after_profile_name_inline
               * @description Insert after profile name some content
               * @input_vars
               * [{"var":"$args","type":"array","desc":"Form Arguments"}]
               * @change_log
               * ["Since: 2.0"]
               * @usage add_action( 'um_after_profile_name_inline', 'function_name', 10, 1 );
               * @example
               * <?php
               * add_action( 'um_after_profile_name_inline', 'my_after_profile_name_inline', 10, 1 );
               * function my_after_profile_name_inline( $args ) {
               *     // your code here
               * }
               * ?>
               */
              do_action( 'um_after_profile_name_inline', $args ); ?>
  
            </div>
          <?php } ?>
  
          <div class="um-clear"></div>
  
          <?php
          /**
           * UM hook
           *
           * @type action
           * @title um_after_profile_header_name_args
           * @description Insert after profile header name some content
           * @input_vars
           * [{"var":"$args","type":"array","desc":"Form Arguments"}]
           * @change_log
           * ["Since: 2.0"]
           * @usage add_action( 'um_after_profile_header_name_args', 'function_name', 10, 1 );
           * @example
           * <?php
           * add_action( 'um_after_profile_header_name_args', 'my_after_profile_header_name_args', 10, 1 );
           * function my_after_profile_header_name_args( $args ) {
           *     // your code here
           * }
           * ?>
           */
          do_action( 'um_after_profile_header_name_args', $args );
          /**
           * UM hook
           *
           * @type action
           * @title um_after_profile_name_inline
           * @description Insert after profile name some content
           * @change_log
           * ["Since: 2.0"]
           * @usage add_action( 'um_after_profile_name_inline', 'function_name', 10 );
           * @example
           * <?php
           * add_action( 'um_after_profile_name_inline', 'my_after_profile_name_inline', 10 );
           * function my_after_profile_name_inline() {
           *     // your code here
           * }
           * ?>
           */
          do_action( 'um_after_profile_header_name' ); ?>
  
        </div>
  
        <?php
  
        $description_key = UM()->profile()->get_show_bio_key( $args );
  
        if ( UM()->fields()->viewing == true && um_user( $description_key ) && $args['show_bio'] ) { ?>
  
          <div class="um-meta-text">
            <?php $description = get_user_meta( um_user( 'ID' ), $description_key, true );
  
            if ( UM()->options()->get( 'profile_show_html_bio' ) ) {
              echo make_clickable( wpautop( wp_kses_post( $description ) ) );
            } else {
              echo esc_html( $description );
            } ?>
          </div>
          <?php if ( ! empty( $args['metafields'] ) ) { ?>
          <div class="um-meta">
  
            <?php echo UM()->profile()->show_meta( $args['metafields'] ); ?>
  
          </div>
          <?php } ?>
            
        <?php } elseif ( UM()->fields()->editing == true && $args['show_bio'] ) { ?>
  
         
  
        <?php } ?>
  
        <div class="um-profile-status <?php echo esc_attr( um_user( 'account_status' ) ); ?>">
          <span><?php printf( __( 'This user account status is %s', 'ultimate-member' ), um_user( 'account_status_name' ) ); ?></span>
        </div>
  
        <?php
        /**
         * UM hook
         *
         * @type action
         * @title um_after_header_meta
         * @description Insert after header meta some content
         * @input_vars
         * [{"var":"$user_id","type":"int","desc":"User ID"},
         * {"var":"$args","type":"array","desc":"Form Arguments"}]
         * @change_log
         * ["Since: 2.0"]
         * @usage add_action( 'um_after_header_meta', 'function_name', 10, 2 );
         * @example
         * <?php
         * add_action( 'um_after_header_meta', 'my_after_header_meta', 10, 2 );
         * function my_after_header_meta( $user_id, $args ) {
         *     // your code here
         * }
         * ?>
         */
        do_action( 'um_after_header_meta', um_user( 'ID' ), $args ); ?>
  
      </div>
      <div class="um-clear"></div>
  
      <?php if ( UM()->fields()->is_error( 'profile_photo' ) ) {
        echo UM()->fields()->field_error( UM()->fields()->show_error( 'profile_photo' ), 'force_show' );
      }
  
      /**
       * UM hook
       *
       * @type action
       * @title um_after_header_info
       * @description Insert after header info some content
       * @input_vars
       * [{"var":"$user_id","type":"int","desc":"User ID"},
       * {"var":"$args","type":"array","desc":"Form Arguments"}]
       * @change_log
       * ["Since: 2.0"]
       * @usage add_action( 'um_after_header_info', 'function_name', 10, 2 );
       * @example
       * <?php
       * add_action( 'um_after_header_info', 'my_after_header_info', 10, 2 );
       * function my_after_header_info( $user_id, $args ) {
       *     // your code here
       * }
       * ?>
       */
      do_action( 'um_after_header_info', um_user( 'ID' ), $args ); ?>
  
    </div>
  
    <?php
  }

  add_action( 'um_profile_custom', 'um_profile_custom', 9 );
