<?php

function nutrican_2020_get_topic_author_link( $args = '' ) { 
 
  // Parse arguments against default values 
  $r = bbp_parse_args( $args, array( 
      'post_id' => 0,  
      'link_title' => '',  
      'type' => 'both',  
      'size' => 80,  
      'sep' => ' ',  
      'show_role' => false 
), 'get_topic_author_link' ); 

  // Used as topic_id 
  if ( is_numeric( $args ) ) { 
      $topic_id = bbp_get_topic_id( $args ); 
  } else { 
      $topic_id = bbp_get_topic_id( $r['post_id'] ); 
  } 

  // Topic ID is good 
  if ( !empty( $topic_id ) ) { 

      // Get some useful topic information 
      $author_url = bbp_get_topic_author_url( $topic_id ); 
      $anonymous = bbp_is_topic_anonymous( $topic_id ); 

      // Tweak link title if empty 
      if ( empty( $r['link_title'] ) ) { 
          $link_title = sprintf( empty( $anonymous ) ? __( 'View %s\'s profile', 'bbpress' ) : __( 'Visit %s\'s website', 'bbpress' ), bbp_get_topic_author_display_name( $topic_id ) ); 

      // Use what was passed if not 
      } else { 
          $link_title = $r['link_title']; 
      } 

      // Setup title and author_links array 
      $link_title = !empty( $link_title ) ? ' title="' . esc_attr( $link_title ) . '"' : ''; 
      $author_links = array(); 

      // Get avatar 
      if ( 'avatar' === $r['type'] || 'both' === $r['type'] ) { 
          $author_links['avatar'] = bbp_get_topic_author_avatar( $topic_id, $r['size'] ); 
      } 

      // Get display name 
      if ( 'name' === $r['type'] || 'both' === $r['type'] ) { 
          $author_links['name'] = '<span class="d-block text-dark-grey">'.bbp_get_topic_author_display_name( $topic_id ) . '</span><span class="d-block text-middle-grey font-size-small">' . get_the_date().'</span>';
      } 

      // Link class 
      $link_class = ' class="bbp-author-' . esc_attr( $r['type'] ) . '"'; 

      // Add links if not anonymous 
      if ( empty( $anonymous ) && bbp_user_has_profile( bbp_get_topic_author_id( $topic_id ) ) ) { 

          // Assemble the links 
          foreach ( $author_links as $link => $link_text ) { 
              $link_class = ' class="bbp-author-' . esc_attr( $link ) . ' text-decoration-none mr-2"'; 
              $open_tag = ($link === 'name') ? '<div>' : '';
              $author_link[] = sprintf( '<a href="%1$s"%2$s%3$s>%4$s</a>', esc_url( $author_url ), $link_title, $link_class, $link_text ); 
          } 

          if ( true === $r['show_role'] ) { 
              $author_link[] = bbp_get_topic_author_role( array( 'topic_id' => $topic_id ) ); 
          } 

          $author_link = implode( $r['sep'], $author_link ); 

      // No links if anonymous 
      } else { 
          $author_link = implode( $r['sep'], $author_links ); 
      } 

  } else { 
      $author_link = ''; 
  } 

  return apply_filters( 'bbp_get_topic_author_link', $author_link, $args ); 
} 

function nutrican_no_view_ip( $author_ip, $r, $args ){
	return __return_empty_string();	
}

add_filter('bbp_get_author_ip','nutrican_no_view_ip', 10, 3 );
