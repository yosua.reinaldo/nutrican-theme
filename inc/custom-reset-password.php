<?php



add_action('login_form_lostpassword', 'redirect_to_custom_lostpassword');
function redirect_to_custom_lostpassword() {
	if ('GET' == $_SERVER['REQUEST_METHOD']) {
		if (is_user_logged_in()) {
			$this->redirect_logged_in_user();
			exit;
		}
		wp_redirect(home_url('forgot-password'));//page slug where reset shortcode will be use
		exit;
	}
}

add_shortcode('custom-password-lost-form', 'render_password_lost_form');
function render_password_lost_form($attributes, $content = null) {
	// Parse shortcode attributes
	$default_attributes = array('show_title' => false);
	$attributes = shortcode_atts($default_attributes, $attributes);


	if (is_user_logged_in()) {
		return __('You are already signed in.', 'personalize-login');
	} else {
		if ( isset( $_REQUEST['errors'] ) ) {
			switch($_REQUEST['errors']){
				case 'empty_username':
					_e( 'You need to enter your email address to continue.', 'personalize-login' );
				case 'invalid_email':
				case 'invalidcombo':
					_e( 'There are no users registered with this email address.', 'personalize-login' );
			}
		}
		if ( isset( $_REQUEST['checkemail'] ) ) {
			switch($_REQUEST['checkemail']){
				case 'confirm':
					_e( 'Password reset email has been sent.', 'personalize-login' );
			}
//			return;
		}
		if ( isset( $_POST['user_login'] ) ) {
			var_dump($_POST['user_login']);
		}
//		$link = get_the_permalink();
		//var_dump($link);
		?>
		<div id="password-lost-form" class="widecolumn">
			<?php if ($attributes['show_title']) : ?>
				<h3><?php _e('Forgot Your Password?', 'personalize-login'); ?></h3>
			<?php endif; ?>

			<p>
				<?php
				_e(
					"Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.",
					'personalize_login'
				);
				?>
			</p>

			<form id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post">
				<p class="form-row">
					<label for="user_login"><?php _e('Email', 'personalize-login'); ?>
						<input type="text" name="user_login" id="user_login">
				</p>

				<p class="lostpassword-submit">
					<input type="submit" name="submit" class="lostpassword-button"
					       value="<?php _e('Reset Password', 'personalize-login'); ?>"/>
				</p>
			</form>
		</div>
		<?php
	}
}

add_action('login_form_lostpassword', 'do_password_lost');
function do_password_lost() {
	if ('POST' == $_SERVER['REQUEST_METHOD']) {
		$errors = retrieve_password();
		if (is_wp_error($errors)) {
			// Errors found
			$redirect_url = home_url('forgot-password');//page slug where reset shortcode will be use
			$redirect_url = add_query_arg('errors', join(',', $errors->get_error_codes()), $redirect_url);
		} else {
			// Email sent
//			$link = get_the_permalink();
//			var_dump($link);
//			$redirect_url = home_url('signin');
			$redirect_url = home_url('forgot-password');//page slug where reset shortcode will be use
			$redirect_url = add_query_arg('checkemail', 'confirm', $redirect_url);
		}

		wp_redirect($redirect_url);
		exit;
	}
}

//After send Email
add_action('login_form_rp', 'redirect_to_custom_password_reset');
add_action('login_form_resetpass', 'redirect_to_custom_password_reset');
function redirect_to_custom_password_reset() {
	if ('GET' == $_SERVER['REQUEST_METHOD']) {
		// Verify key / login combo
		$user = check_password_reset_key($_REQUEST['key'], $_REQUEST['login']);
		if (!$user || is_wp_error($user)) {
			if ($user && $user->get_error_code() === 'expired_key') {
				wp_redirect(home_url('login?login=expiredkey'));
			} else {
				wp_redirect(home_url('login?login=invalidkey'));
			}
			exit;
		}

		$redirect_url = home_url('password-reset');
		$redirect_url = add_query_arg('login', esc_attr($_REQUEST['login']), $redirect_url);
		$redirect_url = add_query_arg('key', esc_attr($_REQUEST['key']), $redirect_url);

		wp_redirect($redirect_url);
		exit;
	}
}


add_shortcode('custom-password-reset-form', 'render_password_reset_form');
function render_password_reset_form($attributes, $content = null) {
	// Parse shortcode attributes
	$default_attributes = array('show_title' => false);
	$attributes = shortcode_atts($default_attributes, $attributes);

	if (is_user_logged_in()) {
		return __('You are already signed in.', 'personalize-login');
	} else {
		if (isset($_REQUEST['login']) && isset($_REQUEST['key'])) {
			$attributes['login'] = $_REQUEST['login'];
			$attributes['key'] = $_REQUEST['key'];

			// Error messages
			$errors = array();
			if (isset($_REQUEST['error'])) {
				$error_codes = explode(',', $_REQUEST['error']);

				foreach ($error_codes as $code) {
					$errors [] = get_error_message($code);
				}
			}
			$attributes['errors'] = $errors;
			?>
			<div id="password-reset-form" class="widecolumn">
				<?php if ($attributes['show_title']) : ?>
					<h3><?php _e('Pick a New Password', 'personalize-login'); ?></h3>
				<?php endif; ?>

				<form name="resetpassform" id="resetpassform"
				      action="<?php echo site_url('wp-login.php?action=resetpass'); ?>" method="post" autocomplete="off">
					<input type="hidden" id="user_login" name="rp_login"
					       value="<?php echo esc_attr($attributes['login']); ?>" autocomplete="off"/>
					<input type="hidden" name="rp_key" value="<?php echo esc_attr($attributes['key']); ?>"/>

					<?php if (count($attributes['errors']) > 0) : ?>
						<?php foreach ($attributes['errors'] as $error) : ?>
							<p>
								<?php echo $error; ?>
							</p>
						<?php endforeach; ?>
					<?php endif; ?>
					<div class="row mb-5">
						<div class="form-group mb-3 col-lg-6">
							<label for="pass1"><?php _e('Password* (terdiri dari huruf kapital, angka dan tanda baca)', 'personalize-login') ?></label>
							<input type="password" name="pass1" id="pass1" class="form-control" size="20" value="" autocomplete="off" required/>
						</div>
						<div class="form-group col-lg-6">
							<label for="pass2"><?php _e('Ulangi Password*', 'personalize-login') ?></label>
							<input type="password" name="pass2" id="pass2" class="form-control" class="input" size="20" value="" autocomplete="off" required equalTo="#pass1"/>
						</div>
					</div>

					<div class="text-center">
						<input type="submit" name="submit" id="resetpass-button" class="btn btn-middle-green btn-long" value="<?php _e('Simpan', 'personalize-login'); ?>"/>
					</div>
				</form>
			</div>
			<?php
		} else {
			return __('Invalid password reset link.', 'personalize-login');
		}
	}
}

add_action('login_form_rp', 'do_password_reset');
add_action('login_form_resetpass', 'do_password_reset');
function do_password_reset() {
	if ('POST' == $_SERVER['REQUEST_METHOD']) {
		$rp_key = $_REQUEST['rp_key'];
		$rp_login = $_REQUEST['rp_login'];

		$user = check_password_reset_key($rp_key, $rp_login);

		if (!$user || is_wp_error($user)) {
			if ($user && $user->get_error_code() === 'expired_key') {
				wp_redirect(home_url('login?login=expiredkey'));
			} else {
				wp_redirect(home_url('login?login=invalidkey'));
			}
			exit;
		}

		if (isset($_POST['pass1'])) {
			if ($_POST['pass1'] != $_POST['pass2']) {
				// Passwords don't match
				$redirect_url = home_url('password-reset');

				$redirect_url = add_query_arg('key', $rp_key, $redirect_url);
				$redirect_url = add_query_arg('login', $rp_login, $redirect_url);
				$redirect_url = add_query_arg('error', 'password_reset_mismatch', $redirect_url);

				wp_redirect($redirect_url);
				exit;
			}

			if (empty($_POST['pass1'])) {
				// Password is empty
				$redirect_url = home_url('password-reset');//page slug where reset shortcode will be use

				$redirect_url = add_query_arg('key', $rp_key, $redirect_url);
				$redirect_url = add_query_arg('login', $rp_login, $redirect_url);
				$redirect_url = add_query_arg('error', 'password_reset_empty', $redirect_url);

				wp_redirect($redirect_url);
				exit;
			}

			// Parameter checks OK, reset password
			reset_password($user, $_POST['pass1']);
			wp_redirect(home_url('login?password=changed'));//page slug where signin shortcode will be use
		} else {
			echo "Invalid request.";
		}

		exit;
	}
}

function get_error_message( $error_code ) {
  switch ( $error_code ) {
    // Login errors

    case 'empty_username':
      return __( 'You do have an email address, right?', 'personalize-login' );

    case 'empty_password':
      return __( 'You need to enter a password to login.', 'personalize-login' );

    case 'invalid_username':
      return __(
        "We don't have any users with that email address. Maybe you used a different one when signing up?",
        'personalize-login'
      );

    case 'incorrect_password':
      $err = __(
        "The password you entered wasn't quite right. <a href='%s'>Did you forget your password</a>?",
        'personalize-login'
      );
      return sprintf( $err, wp_lostpassword_url() );

    // Registration errors

    case 'email':
      return __( 'The email address you entered is not valid.', 'personalize-login' );

    case 'email_exists':
      return __( 'An account exists with this email address.', 'personalize-login' );

    case 'closed':
      return __( 'Registering new users is currently not allowed.', 'personalize-login' );

    case 'captcha':
      return __( 'The Google reCAPTCHA check failed. Are you a robot?', 'personalize-login' );

    // Lost password

    case 'empty_username':
      return __( 'You need to enter your email address to continue.', 'personalize-login' );

    case 'invalid_email':
    case 'invalidcombo':
      return __( 'There are no users registered with this email address.', 'personalize-login' );

    // Reset password

    case 'expiredkey':
    case 'invalidkey':
      return __( 'The password reset link you used is not valid anymore.', 'personalize-login' );

    case 'password_reset_mismatch':
      return __( "The two passwords you entered don't match.", 'personalize-login' );

    case 'password_reset_empty':
      return __( "Sorry, we don't accept empty passwords.", 'personalize-login' );

    default:
      break;
  }

  return __( 'An unknown error occurred. Please try again later.', 'personalize-login' );
}