<?php

function get_recent_posts($atts) {
  ob_start();
  global $post;
	$a = shortcode_atts(array(
		'cat'     => ''
  ), $atts);

	$args = array(
		'category'        => $a['cat'],
		'numberposts' 		=> 8,
		'orderby'         => 'date',
    'order'           => 'DESC',
	);

	echo '<div class="row">';
	$posts = get_posts($args);
	foreach($posts as $post) {
    setup_postdata($post);
    get_template_part('template-parts/content-loop');
  }
  echo '</div><!-- /.row -->';
	wp_reset_postdata();
	return ob_get_clean();
}

add_shortcode('recent_posts', 'get_recent_posts');

function get_recent_faq($atts) {
  ob_start();
  $cargs = array(
    'child_of'      => 0,
    'orderby'       => 'name',
    'order'         => 'ASC',
    'hide_empty'    => 1,
    'taxonomy'      => 'faq_categories',
  );
  echo '<div class="accordion" id="accordionFAQ">';
  foreach(get_categories($cargs) as $key => $cat) {
    $args = array(
      'post_type' => 'faq',
      'post_status' => 'publish',
      'orderby' => 'DATE',
      'order' => 'ASC',
      'tax_query' =>
        array(
          array(
            'taxonomy' => 'faq_categories',
            'field'    => 'id',
            'terms'    => $cat->term_id
          ),
        ),
      // 's' => $keyword
    );
    echo '<div class="card">
      <div class="card-header px-0" id="heading-'.$key.'">
        <button class="btn btn-link w-100 text-left px-0 d-flex justify-content-between" type="button" data-toggle="collapse" data-target="#collapse-'.$key.'" aria-expanded="false" aria-controls="collapse-'.$key.'">
          '.$cat->name.'
          <i class="ml-3 fas fa-angle-down text-light-green"></i>
        </button>
      </div>
      <div id="collapse-'.$key.'" class="collapse" aria-labelledby="heading-'.$key.'" data-parent="#accordionFAQ">
        <div class="card-body p-0">';
    $loop = new WP_Query( $args );
    nutrican_2020_list_questions($loop);
    echo '</div></div></div>';
  }
  echo '</div>';
  wp_reset_postdata();
	return ob_get_clean();
}

add_shortcode('recent_faq', 'get_recent_faq');

function get_recent_questions($atts) {
  ob_start();
  $args = array(
    'post_type' => 'faq',
    'post_status' => 'publish',
    'orderby' => 'DATE',
    'order' => 'ASC',
  );
  $loop = new WP_Query( $args );
  echo '<div class="accordion">';
  while ( $loop->have_posts() ) : $loop->the_post();
    $slug = basename(get_permalink(get_the_ID()));
    echo '<div class="card">';
		the_title('<div class="card-header px-0"><button class="btn btn-link w-100 text-left px-0 border-radius-0 text-black border-radius-0 d-flex justify-content-between text-decoration-none" data-toggle="collapse" href="#'.$slug.'" role="button" aria-expanded="false">', '<i class="ml-3 fas fa-angle-down text-light-green"></i></button></div>', true);
		echo '<div class="collapse multi-collapse" id="'.$slug.'">
			<div class="card card-body px-0">
				'.get_the_content().'
			</div>
    </div>';
    echo '</div>';
  endwhile;
  echo '</div>';
  return ob_get_clean();
}

add_shortcode('recent_questions', 'get_recent_questions');

function post__shortcode( $atts ) {
  $a = shortcode_atts(
      array (
          'id'   => false,
          'type' => "content",
      ), $atts );

  $id   = $a [ 'id' ];
  $type = $a [ 'type' ];

  // bad id
  if ( ! is_numeric( $id ) ) {
      return '';
  }

  // find the post
  $post = get_post( $id );

  // bad post
  if ( ! $post ) {
      return '';
  }

  // allow for other post attributes
  switch ( $type ) {

      case "content":
          return $id === get_the_ID() || $id === get_queried_object_id()
              ? '' // no recursive loops!
              : apply_filters( 'the_content', $post->post_content );

      case "title":
          return $post->post_title;
  }

  // nothing to see here
  return '';
}

add_shortcode( 'post', 'post__shortcode' );