<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package nutrican_2020
 */

if ( ! function_exists( 'nutrican_2020_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function nutrican_2020_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Diposting: %s', 'post date', 'nutrican-2020' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	}
endif;

if ( ! function_exists( 'nutrican_2020_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function nutrican_2020_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'nutrican-2020' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	}
endif;

if ( ! function_exists( 'nutrican_2020_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function nutrican_2020_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'nutrican-2020' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'nutrican-2020' ) . '</span>', $categories_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'nutrican-2020' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'nutrican-2020' ) . '</span>', $tags_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'nutrican-2020' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'nutrican-2020' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'nutrican_2020_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function nutrican_2020_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

			<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
				<?php
					the_post_thumbnail(
						'post-thumbnail',
						array(
							'alt' => the_title_attribute(
								array(
									'echo' => false,
								)
							),
						)
					);
				?>
			</a>

			<?php
		endif; // End is_singular().
	}
endif;

if ( ! function_exists( 'wp_body_open' ) ) :
	/**
	 * Shim for sites older than 5.2.
	 *
	 * @link https://core.trac.wordpress.org/ticket/12563
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
endif;


if ( ! function_exists( 'nutrican_2020_resize_text' ) ) :
	/**
	 * Text resize function
	 */
	function nutrican_2020_resize_text() {
		echo '<div class="resize-text">
			<label class="d-block mb-2 font-weight-semi text-light-green">Perbesar Huruf</label>
			<div class="d-flex">
				<button class="btn btn-link btn-outline plus mr-3"><i class="fas fa-plus"></i></button>
				<button class="btn btn-link btn-outline minus"><i class="fas fa-minus"></i></button>
			</div>
		</div>
		';

	}
endif;

if ( ! function_exists( 'nutrican_2020_share' ) ) :
	/**
	 * Share function
	 */
	function nutrican_2020_share() {
		echo '<div class="addthis_toolboxshare-action">
			<label class="mb-2 font-weight-semi text-light-green d-none d-lg-block">Bagikan</label>
			<div class="d-flex align-items-center">
				<div class="row mx-0 d-none d-lg-block">
					<button class="btn btn-link btn-outline">
						<a class="addthis_button_facebook">
							<i class="fab fa-facebook-f"></i>
						</a>
					</button>
					<button class="btn btn-link btn-outline ml-3">
						<a class="addthis_button_whatsapp">
							<i class="fab fa-whatsapp	"></i>
						</a>
					</button>
					<button class="btn btn-link btn-outline ml-3">
						<a class="addthis_button_link">
							<i class="fas fa-link"></i>
						</a>
					</button>
				</div>
				<div class="d-lg-none">
					<button class="btn btn-link btn-outline toggle-share">Bagikan</button>
				</div>
			</div>
		</div>';

	}
endif;

if ( ! function_exists( 'nutrican_2020_mobile_share' ) ) :
	/**
	 * Share function
	 */
	function nutrican_2020_mobile_share() {
		echo '<div class="overlay-mobile-share d-none">
		<div class="overlay"></div>
		<div class="bottom-sheets">
			<div class="addthis_toolbox mobile_share py-3 px-4">
				<ul class="list-group list-group-flush ml-0">
					<li class="list-group-item px-0 text-center position-relative">
						<strong>Bagikan ke</strong>
						<button class="btn btn-link close"><i class="fas fa-times"></i></button>
					</li>
					<li class="list-group-item px-0">
						<a class="addthis_button_whatsapp"><span>Whatsapp</span></a>
					</li>
					<li class="list-group-item px-0">
						<a class="addthis_button_facebook"><span>Facebook</span></a>
					</li>
					<li class="list-group-item px-0">
						<a class="addthis_button_link"><span>Salin Link</span></a>
					</li>
				</ul>
			</div>
		</div>
		</div>
		';
	}
endif;

function nutrican_2020_list_questions($loop){
	while ( $loop->have_posts() ) : $loop->the_post();
		$slug = basename(get_permalink(get_the_ID()));
		the_title('<button class="btn btn-link w-100 text-left px-0 border-bottom border-radius-0 text-black border-radius-0" data-toggle="collapse" href="#'.$slug.'" role="button" aria-expanded="false">', '</button>', true);
		echo '<div class="collapse multi-collapse" id="'.$slug.'">
			<div class="card card-body px-0">
				'.get_the_content().'
			</div>
		</div>';
	endwhile;
}